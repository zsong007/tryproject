package zhesong.madcourse.neu.edu.charmingu;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class StepCountActivity extends AppCompatActivity {
    private float step_number;
    private TextView Step_Textview;
    private Button button_return;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_step);

        Step_Textview = (TextView) findViewById(R.id.text_step);

        Bundle b = getIntent().getExtras();
        step_number = b.getFloat("steps");

        System.out.println(step_number);
        Step_Textview.setText(String.valueOf(step_number));

        button_return = (Button) findViewById(R.id.button_return);
        button_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StepCountActivity.this, MainActivity.class));
            }
        });


    }

}