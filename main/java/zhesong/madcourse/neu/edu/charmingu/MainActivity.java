package zhesong.madcourse.neu.edu.charmingu;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity {


    private ToggleButton tg_btn;
    private Button btn_step;
    private SensorManager mSensorManager;
  private AlertDialog mDialog;
    private Sensor mStepDetector;
    private float step_number;
    private Button btn_map;
    private Button btn_map2;
    private Button btn_map3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mStepDetector = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        if (mStepDetector != null) {
            System.out.println("Success! There is step_detector sensor in this device!!!!");

        } else {
            System.out.println("Old Phone!!!");
          AlertDialog.Builder builder = new AlertDialog.Builder(this);
             builder.setTitle("Warning!");
             builder.setMessage("Sorry, CharmingU can not enabled step counter service for you since " +
                     "there is no hardware sensor in this device...:(");
             builder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
                 @Override
                 public void onClick(DialogInterface dialogInterface, int i) {

                 }
             });
            mDialog = builder.show();
            tg_btn.setEnabled(false);
            btn_step.setEnabled(false);
        }






    }
    private void initialize(){
        step_number=0;
        tg_btn = (ToggleButton) findViewById(R.id.button_toggle);
        tg_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub
//
                if (isChecked) {
                    registerSensorListener();
                    System.out.println(step_number);

                } else {
                    unregisterSensorListener();

                }
            }

        });

        btn_step = (Button) findViewById(R.id.button_step);
        btn_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this,StepCountActivity.class);
                Bundle b = new Bundle();
                b.putFloat("steps",step_number);
                intent.putExtras(b);
                startActivity(intent);

            }

        });



        btn_map2 = (Button) findViewById(R.id.button_map2);
        btn_map2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, GoogleMapActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private SensorEventListener mSensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            if (sensorEvent.sensor == mStepDetector) {
                step_number += sensorEvent.values[0];
                System.out.println(step_number);
            }
            else {
                System.out.println("fail!");
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
        };


        public void registerSensorListener() {
            mSensorManager.registerListener(mSensorEventListener, mStepDetector, SensorManager.SENSOR_DELAY_NORMAL);
        }

        public void unregisterSensorListener() {
            mSensorManager.unregisterListener(mSensorEventListener);
        }


}
